# Site parser

## Project description

Parse info about mobile phones.

## Dependencies

* Ruby 2.3
* Rails 4

## Quick Start

```bash
# clone repo
git clone git@bitbucket.org:ruslanimos/parse-gsm-arena.git
cd parse-gsm-arena

# run setup script
bin/setup
```

## Scripts

* `bin/setup` - setup required gems and migrate db if needed
* `bin/quality` - run code quality tools for the app
* `bin/ci` - should be used in the CI to run specs

## App url

* https://parse-gsm-arena.herokuapp.com
