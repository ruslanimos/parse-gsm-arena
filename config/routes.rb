Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: "users/registrations" }
  resources :phones, only: %i(index)
  get "/phone/", to: "phones#show"
  resources :search, only: %i(index)
  root to: "pages#home"
end
