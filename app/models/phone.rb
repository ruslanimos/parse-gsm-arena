class Phone
  attr_reader :network, :battery, :platform, :tests

  def initialize(network, battery, platform, tests)
    @network  = network
    @battery  = battery
    @platform = platform
    @tests    = tests
  end
end
