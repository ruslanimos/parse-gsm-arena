class SearchController < ApplicationController
  expose(:phones) { RemoteSearch.call(query: search_params[:query]).results }

  def index
  end

  private

  def search_params
    params.permit(:query)
  end
end
