class PagesController < ApplicationController
  expose(:makers) { FetchMakers.call.makers }

  def home
  end
end
