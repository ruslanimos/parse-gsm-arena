class PhonesController < ApplicationController
  expose(:models) { models_list }
  expose(:phone) { FetchPhone.call(model_url: phone_params[:model_url]).phone }

  def index
  end

  def show
  end

  private

  def models_list
    list = FetchModels.call(maker_path: phone_params[:maker_path]).models
    list.map { |m| [m[:name], m[:url]] }
  end

  def phone_params
    params.permit(:model_url, :maker_path)
  end
end
