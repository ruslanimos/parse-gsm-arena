module PhonesHelper
  def truncated(string, length = 53)
    string.truncate(length) if string.present?
  end
end
