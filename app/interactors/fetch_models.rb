require 'nokogiri'
require 'open-uri'

class FetchModels
  include Interactor

  BASE_URL = "http://www.gsmarena.com".freeze

  delegate :maker_path, to: :context

  def call
    [].tap do |models|
      pages.each do |page|
        models |= document(page).css(".makers li").map { |model| fetch_info(model) }
      end

      context.models = models
    end
  end

  private

  def pages
    [].tap do |list|
      doc = document(maker_path)
      list << maker_path

      doc.css(".nav-pages a").map do |link|
        list << link.attributes["href"].value
      end
    end
  end

  def document(path)
    Nokogiri::HTML(
      open(full_uri(path))
    )
  end

  def full_uri(path)
    URI.join(BASE_URL, path)
  end

  def fetch_info(model)
    link = model.children.first
    name = link.css("span").first.text
    path = link.attributes["href"].value
    { name: name, url: full_uri(path).to_s }
  end
end
