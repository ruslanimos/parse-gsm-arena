require 'nokogiri'
require 'open-uri'

class FetchPhone
  include Interactor

  SECTIONS = {
    network:   0,
    platform:  4,
    battery:  10,
    tests:    12
  }.freeze

  delegate :model_url, to: :context

  def call
    context.phone = Phone.new(*phone_params)
  end

  private

  def phone_params
    [network, battery, platform, tests]
  end

  def network
    section = specs_list[SECTIONS[:network]]
    section.css("tr.tr-hover td.nfo a").text if section
  end

  def battery
    section = specs_list[SECTIONS[:battery]]
    section.css("tr td.nfo").map(&:text).join(", ") if section
  end

  def platform
    section = specs_list[SECTIONS[:platform]]
    section.css("tr td.nfo").map(&:text).join(", ") if section
  end

  def tests
    section = specs_list[SECTIONS[:tests]]
    section.css("tr td.nfo a").map(&:text).join(", ") if section
  end

  def specs_list
    @specs_list ||= Nokogiri::HTML(open(model_url)).css("#specs-list table")
  end
end
