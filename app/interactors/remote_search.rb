require 'nokogiri'
require 'open-uri'

class RemoteSearch
  include Interactor

  BASE_URL = "http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=".freeze

  def call
    context.results = search_results
  end

  private

  def search_results
    return [] if context.query.blank?
    doc.css("#review-body .makers li span").map(&:text)
  end

  def doc
    Nokogiri::HTML(open(search_url))
  end

  def search_url
    BASE_URL + query
  end

  def query
    context.query.strip.gsub(/\s+/, "+")
  end
end
