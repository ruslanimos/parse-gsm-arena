require 'nokogiri'
require 'open-uri'

class FetchMakers
  include Interactor

  MAKERS_URL = "http://www.gsmarena.com/makers.php3".freeze

  def call
    context.makers = makers_list
  end

  private

  def makers_list
    {}.tap do |h|
      doc.css("tbody", "tr td").each do |td|
        el = td.children.first
        h[el.text] = el.attributes["href"].value if el.text.present?
      end
    end
  end

  def doc
    Nokogiri::HTML(open(MAKERS_URL))
  end
end
