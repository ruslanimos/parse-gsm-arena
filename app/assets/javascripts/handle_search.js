$(function() {
  var timerId = null;

  $("#query").on("keyup", function(e){
    clearTimeout(timerId);
    timerId = setTimeout(search, 500);
  });

  function search() {
    var query = $("#query").val();
    $.get("/search", { query: query });
  }
});
