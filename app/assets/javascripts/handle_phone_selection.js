$(function() {
  $("#makers").on("change", function(e) {
    var maker_path = $("#makers option:selected").val();
    $.get("phones", { maker_path: maker_path });
  });

  $(document).on("change", "#models", function(e) {
    var model_url = $("#models option:selected").val();
    $.get("phone", { model_url: model_url });
  });
});
